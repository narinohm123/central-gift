import React, { useState } from "react"
import {
  Box,
  Center,
  Image,
  Text,
  Flex,
  Button
} from "@chakra-ui/react"
import { useRouter } from 'next/router'

function Privilege() {
  return (
    <>
      <Box
        backgroundImage="url('/image/bgstart.jpg')"
        backgroundPosition={["center", "center", "center", "center", "center", "center"]}
        backgroundSize="cover"
        w="100%"
        h="100vh"
      >
        <Center pt="2rem">
          <Image w={["30%", "30%", "20%", "15%", "15%", "10%"]} alt='text' src="/image/topic.png" objectFit='cover' />
        </Center>
        <Flex mt={"1rem"} alignItems="center" justifyContent="center">
          <Flex
            textAlign="center"
            w="250px"
            h="150px"
            bg="#EC1C24"
            borderRight="4px"
            borderBottom="4px"
            flexDirection='column'
            alignItems="center"
            justifyContent="center"
          >
            <Text color="white" fontSize="20px" fontFamily="CPN-Bold">THE GREATEST</Text>
            <Text w="full" fontSize="38px" h="50px" bg="white" fontFamily="CPN-Bold" color="#EC1C24">GRAND</Text>
            <Text mt="-0.5rem" color="white" h="65px" fontSize="60px" fontFamily="CPN-Bold">SALE</Text>
          </Flex>

        </Flex>
        <Flex mt={["2.5rem", "2rem", "10rem", "6rem", "6rem", "6rem"]} flexDirection="column" justifyContent="center" alignItems="center">
          <Flex flexDirection="column" justifyContent="center" alignItems="center" fontFamily="CPN-Bold" pb="2rem">
            <Text fontSize={["44px","54px","80px","44px","44px","44px"]}
            webkitTextStroke="2px white"
            >คุณใช้สิทธิ์</Text>
            <Text fontSize={["44px","54px","80px","44px","44px","44px"]}
            webkitTextStroke="2px white"
            >ไปครบแล้ว</Text>
            <Text color="white" fontSize={["20px","20px","38px","20px","20px","20px"]}>ร่วมสนุกกันใหม่ครั้งหน้า</Text>
          </Flex>
          <Button color="#EC1C24" fontFamily="CPN-Bold" fontSize="30px"
            py="2rem" px="4rem" borderRadius="30px"
          >
            ปิด
          </Button>
        </Flex>
      </Box>
    </>
  )
}

export default Privilege