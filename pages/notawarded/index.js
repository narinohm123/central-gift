import React, { useEffect } from 'react'
import {
    Box,
    Center,
    Image,
    Text,
    Flex,
    Button
} from "@chakra-ui/react"
import confetti from 'canvas-confetti'

function Notawarded() {
    useEffect(() => {
        confetti({
            particleCount: 400,
            spread: 350,
            origin: { y: 0.5 }
        })
    }, [])
    return (
        <>
            <Box
                backgroundImage="url('/image/bgnot.jpg')"
                backgroundPosition={["center", "center", "center", "center", "center", "center"]}
                backgroundSize="cover"
                w="100%"
                h="100vh"
                display="flex"
                flexDirection="column"
                justifyContent="space-between"
            >
                <Center pt="2rem"
                    display="flex"
                    flexDirection="column"
                    justifyContent="space-between"
                >
                    <Image w={["30%", "20%", "15%", "10%", "10%", "10%"]} pb="2rem" alt='text' src="/image/topic.png" objectFit='cover' />

                    <Flex mt="0rem" alignItems="center" justifyContent="center">
                        <Flex
                            textAlign="center"
                            w="250px"
                            h="150px"
                            bg="#EC1C24"
                            borderRight="4px"
                            borderBottom="4px"
                            flexDirection='column'
                            alignItems="center"
                            justifyContent="center"
                        >
                            <Text color="white" fontSize="20px" fontFamily="CPN-Bold">THE GREATEST</Text>
                            <Text w="full" fontSize="38px" h="50px" bg="white" fontFamily="CPN-Bold" color="#EC1C24">GRAND</Text>
                            <Text mt="-0.5rem" color="white" h="65px" fontSize="60px" fontFamily="CPN-Bold">SALE</Text>
                        </Flex>

                    </Flex></Center>
                <Flex mt="-1rem" w="full" flexDirection="column" justifyContent="center" alignItems="center">
                    <Box w={["10rem","12rem","12rem","12rem","13rem","15rem"]}>
                    <div className='button'>
                        <Image alt='box' src="/image/icon.png" w="100%" />
                    </div>
                    </Box>
                    <Image alt="text" mt="1rem" src="/image/cannot.png" h={["100px" ,"110px" ,"110px" ,"120px" ,"130px" ,"150px" ]}/>
                    <Text color="white" fontSize={["24px","24px","24px","24px","24px","36px"]} fontFamily="CPN-Bold">ร่วมสนุกกันใหม่ครั้งหน้า</Text>
                </Flex>
                <Center pb="3rem">
                    <Button className='animated-background' color="#EC1C24" fontFamily="CPN-Bold" fontSize="30px" py="2rem" px="4rem" borderRadius="30px">
                        ปิด
                    </Button>
                </Center>
            </Box>
            <style jsx>{
                `
                .button {
                    animation: wiggle 2s linear infinite;
                  }
                  @keyframes wiggle {
                    0%, 7% {
                      transform: rotateZ(0);
                    }
                    15% {
                      transform: rotateZ(-15deg);
                    }
                    20% {
                      transform: rotateZ(10deg);
                    }
                    25% {
                      transform: rotateZ(-10deg);
                    }
                    30% {
                      transform: rotateZ(15deg);
                    }
                    35% {
                      transform: rotateZ(-10deg);
                    }
                    40%, 100% {
                      transform: rotateZ(0);
                    }
                  }
                  .button {
                    height: auto;
                    width: 100%;
                    display:flex;
                    alignItems:center;
                    justifyContent:center;
                    border: none;
                    border-radius: 0 0 0.2em 0.2em;
                    font-family: Helvetica, Arial, Sans-serif;
                    font-size: 1em;
                    transform-origin: 10% 6em;
                  }
                
                `
            }</style>
        </>
    )
}

export default Notawarded