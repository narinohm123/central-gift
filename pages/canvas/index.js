import React, { useState, useEffect } from 'react'
import {
    Box,
    Center,
    Image,
    Text,
    Flex,
    Button,
    useDisclosure,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
} from "@chakra-ui/react"
import confetti from 'canvas-confetti'

function Canvas() {
    const { isOpen, onOpen, onClose } = useDisclosure()
    useEffect(() => {
        confetti({
            particleCount: 400,
            spread: 350,
            origin: { y: 0.5 }
        })
    }, [])
    return (
        <>
            <Box
                backgroundImage="url('/image/bgnot.jpg')"
                backgroundPosition={["center", "center", "center", "center", "center", "center"]}
                backgroundSize="cover"
                w="100%"
                h="100vh"
                display="flex"
                flexDirection="column"
                justifyContent="space-between"
            >
                <Center pt="1 rem"
                    display="flex"
                    flexDirection="column"
                    justifyContent="space-between">
                    <Image w={["30%", "20%", "20%", "10%", "10%", "10%"]} alt='text' src="/image/topic.png" objectFit='cover' pt="1rem" />
                    <Flex mt="1rem" alignItems="center" justifyContent="center">
                        <Flex
                            textAlign="center"
                            w="250px"
                            h="150px"
                            bg="#EC1C24"
                            borderRight="4px"
                            borderBottom="4px"
                            flexDirection='column'
                            alignItems="center"
                            justifyContent="center"
                        >
                            <Text color="white" fontSize="20px" fontFamily="CPN-Bold">THE GREATEST</Text>
                            <Text w="full" fontSize={["34px", "38px", "38px", "38px", "38px", "38px"]} h={["45px", "50px", "50px", "50px", "50px", "50px"]} bg="white" fontFamily="CPN-Bold" color="#EC1C24">GRAND</Text>
                            <Text mt="-0.5rem" color="white" h={["45px", "65px", "65px", "65px", "65px", "65px"]} fontSize={["50px", "60px", "60px", "60px", "60px", "60px"]} fontFamily="CPN-Bold">SALE</Text>
                        </Flex>
                    </Flex>
                </Center>
                <Flex mt="0rem" w="full" flexDirection="column" justifyContent="center" alignItems="center">
                    {/* <Image alt='box' src="/image/youwon.png" w={["40%", "30%", "20%", "20%", "20%", "15%"]} /> */}
                    <Center color="black" fontSize="30px" fontFamily="CPN-Bold" fontWeight="bold">
                        <Text
                            textShadow="-2px 0px white, 0px 2px white, 2px 0px white, 0px -2px white"
                            color="black"
                        >YOU WON!!</Text>
                    </Center>
                    <Image alt='box' src="/image/canvascutebag.png" w={["60%", "40%", "30%", "30%", "20%", "20%"]} />
                    {/* <Image alt='box' src="/image/testproduct.png" w={["60%", "30%", "30%", "20%", "20%", "20%"]} /> */}
                    <Center color="black" fontSize="30px" fontFamily="CPN-Bold">
                        <Text
                            textShadow="-2px 0px white, 0px 2px white, 2px 0px white, 0px -2px white"
                            color="black"
                        >Canvas Cute Bag</Text>
                    </Center>
                </Flex>
                <Center mb="2rem">
                    <Button className='animated-background' color="#EC1C24" fontFamily="CPN-Bold" fontSize={["26px", "30px", "30px", "30px", "30px", "30px"]} py="2rem" px={["2rem", "4rem", "4rem", "4rem", "4rem", "4rem"]} borderRadius="30px"
                        onClick={onOpen}
                    >
                        แลกรางวัล
                    </Button>
                </Center>
                <Modal onClose={onClose} size="auto" isOpen={isOpen} isCentered>
                    <ModalOverlay />
                    <ModalContent
                        backgroundImage="url('/image/bgstart.jpg')"
                        backgroundPosition={["center", "center", "center", "center", "center", "center"]}
                        backgroundSize="cover"
                        w={["90%", "90%", "90%", "90%", "90%", "90%"]}
                        h="max-content"
                        display="flex"
                        flexDirection="column"
                        // justifyContent="space-between"
                        alignItems="center"
                    >
                        <ModalCloseButton border="2px" borderRadius="50%" bg="white" />
                        <Image w={["30%", "20%", "20%", "40%", "10%", "10%"]} alt='text' src="/image/topic.png" objectFit='cover' pt={["1rem", "1rem", "3rem", "1rem", "1rem", "1rem"]} />
                        <Flex flexDirection="column" alignItems="center" justifyContent="center" fontFamily="CPN-Bold" fontSize={["16px", "16px", "22px", "28px", "28px", "28px"]} color="white" mt="1rem">
                            <Text>ยินดีด้วย</Text>
                            <Text>คุณได้รับ Canvas Cute Bag</Text>
                            <Text>จำนวน 1 รางวัล</Text>
                        </Flex>
                        <Image alt='box' src="/image/canvascutebag.png" w={["45%", "40%", "40%", "50%", "20%", "20%"]} />
                        <Flex flexDirection="column" alignItems="center" justifyContent="center" fontFamily="CPN-Bold" fontSize={["12px", "14px", "20px", "12px", "12px", "12px"]} color="white" mt="0rem" textAlign="center">
                            <Text>กรุณาแคปหน้าจอนี้ แล้วนำไปแสดง</Text>
                            <Text color="#FCEE21">ที่เคาท์เตอร์ประชาสัมพันธ์ ชั้น 1</Text>
                            <Text color="#FCEE21">ศูนย์การค้าเซ็นทรัล เชียงใหม่</Text>
                            <Text >ภายในวันที่ 20/6/65- 15/7/65</Text>
                        </Flex>
                        <Flex w="full" h="full" alignContent="center" justifyContent="center" pt="0.5rem" fontFamily="CPN-Bold" >
                            <Box bg="rgb(255,255,255,0.9)" borderTopRadius="20px" w={["90%", "80%", "95%", "80%", "80%", "80%"]} pb={["0rem", "0rem", "1rem", "0rem", "0rem", "0rem"]} h={["full", "full", "max-content", "full", "full", "full"]}>
                                <Text w="full" textAlign="center" mt="1rem" fontSize={["14px", "14px", "22px", "14px", "14px", "14px"]}>เงื่อนไข</Text>
                                <Flex flexDirection="column" fontSize={["11px", "12px", "18px", "10px", "10px", "10px"]} pt={["0rem", "0.5rem", "0rem", "0rem", "0rem", "0rem"]}>
                                    <Text ml="1rem">• 1 ท่าน สามารถเล่นเกมส์ได้ 1 ครั้ง / กิจกรรม สามารถเล่นได้</Text>
                                    <Text ml="1rem">&#160;ตั้งแต่วันที่ 15/6/65-30/6/65</Text>
                                    <Text ml="1rem">• ผู้ที่ได้รับรางวัล Smart watch จะต้องมีใบเสร็จยอดบิล </Text>
                                    <Text ml="1rem">&#160;500 บาทขึ้นไป นำแสดงตอนแลกรับของรางวัล</Text>
                                    <Text ml="1rem">• กิจกรรมนี้เป็นกิจกรรมเฉพาะศูนย์การค้าเซ็นทรัล เชียงใหม่ </Text>
                                    <Text ml="1rem">&#160;เท่านั้น</Text>
                                    <Text ml="1rem">• เงื่อนไขเป็นไปตามบริษัทฯ กำหนด ขออนุญาติสงวนสิทธิ์</Text>
                                    <Text ml="1rem">&#160;ในการเปลี่ยนแปลงเงื่อนไขโดยไม่ต้องแจ้งล่วงหน้า</Text>
                                    <Text ml="1rem">• สอบถามข้อมูลเพิ่มเติมโทร 053-998-999 ต่อ 1303</Text>
                                </Flex>
                            </Box>
                        </Flex>
                    </ModalContent>
                </Modal>
            </Box>
            <style jsx>{
                `
                .button {
                    animation: wiggle 2s linear infinite;
                  }
                  @keyframes wiggle {
                    0%, 7% {
                      transform: rotateZ(0);
                    }
                    15% {
                      transform: rotateZ(-15deg);
                    }
                    20% {
                      transform: rotateZ(10deg);
                    }
                    25% {
                      transform: rotateZ(-10deg);
                    }
                    30% {
                      transform: rotateZ(6deg);
                    }
                    35% {
                      transform: rotateZ(-4deg);
                    }
                    40%, 100% {
                      transform: rotateZ(0);
                    }
                  }
                  .button {
                    height: auto;
                    width: 8rem;
                    display:flex;
                    alignItems:center;
                    justifyContent:center;
                    border: none;
                    border-radius: 0 0 0.2em 0.2em;
                    font-family: Helvetica, Arial, Sans-serif;
                    font-size: 1em;
                    transform-origin: 10% 6em;
                  }
                
                `
            }</style>
        </>
    )
}

export default Canvas