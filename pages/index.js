import React, { useState } from "react"
import {
  Box,
  Center,
  Image,
  Text,
  Flex,
  Button
} from "@chakra-ui/react"
import { useRouter } from 'next/router'

export default function Home() {
  const [first, setFirst] = useState(true)
  const [second, setSecond] = useState(false)
  const router = useRouter()
  return (
    <>
      <Box
        backgroundImage="url('/image/bgstart.jpg')"
        backgroundPosition={["center", "center", "center", "center", "center", "center"]}
        backgroundSize="cover"
        w="100%"
        h="100vh"
      >
        <Center pt="2rem">
          <Image w={["30%", "30%", "20%", "15%", "15%", "10%"]} alt='text' src="/image/topic.png" objectFit='cover' />
        </Center>
        <Flex mt={"1rem"} alignItems="center" justifyContent="center">
          <Flex
            textAlign="center"
            w="250px"
            h="150px"
            bg="#EC1C24"
            borderRight="4px"
            borderBottom="4px"
            flexDirection='column'
            alignItems="center"
            justifyContent="center"
          >
            <Text color="white" fontSize="20px" fontFamily="CPN-Bold">THE GREATEST</Text>
            <Text w="full" fontSize="38px" h="50px" bg="white" fontFamily="CPN-Bold" color="#EC1C24">GRAND</Text>
            <Text mt="-0.5rem" color="white" h="65px" fontSize="60px" fontFamily="CPN-Bold">SALE</Text>
          </Flex>

        </Flex>
        {first == true ? (
          <>
            <Center mt={["2rem", "5rem", "5rem", "5rem", "5rem", "6rem"]}>
              <button className='button'>
                <Image alt='box' src="/image/box.png" w="18rem" />
              </button>
            </Center>
            <Center mt={["2.5rem", "6rem", "6rem", "6rem", "6rem", "6rem"]}>
             {/* <div className="color"> */}
              <Button color="#EC1C24" fontFamily="CPN-Bold" fontSize="30px"
                py="2rem" px="4rem" borderRadius="30px"
                
                onClick={async () => {
                  await setFirst(false)
                  await setSecond(true)
                  await setTimeout(() => {
                    router.push('/warded')
                  }, 1500)

                }}
              >
                เล่น
              </Button>
              {/* </div> */}
            </Center>
          </>
        ) : (
          <>
            <Center mt={["2rem", "5rem", "5rem", "5rem", "5rem", "6rem"]}>
              <button className='buttondis'>
                <div className="move">
                  <Image
                    alt='box'
                    transform="perspective(2000px) rotatex(-40deg) scale(1.2) translateX(0rem) "
                    src="/image/top.png" w="18rem" />
                </div>
                <Center h="2rem" zIndex="banner">
                  <div className="light"></div>
                </Center>
                <Image alt='box' zIndex="overlay" src="/image/button.png" w="18rem" />
              </button>
            </Center>

          </>
        )}

      </Box>
      <style jsx>{
        `
       
        .button {
          animation: wiggle 2s linear infinite;
        }
        @keyframes wiggle {
          0%, 7% {
            transform: rotateZ(0);
          }
          15% {
            transform: rotateZ(-15deg);
          }
          20% {
            transform: rotateZ(10deg);
          }
          25% {
            transform: rotateZ(-10deg);
          }
          30% {
            transform: rotateZ(6deg);
          }
          35% {
            transform: rotateZ(-4deg);
          }
          40%, 100% {
            transform: rotateZ(0);
          }
        }
        .button {
          height: auto;
          width: 16em;
          border: none;
          border-radius: 0 0 0.2em 0.2em;
          font-family: Helvetica, Arial, Sans-serif;
          font-size: 1em;
          transform-origin: 50% 5em;
          
        }

        .buttondis {
          height: auto;
          width: 12em;
          border: none;
          border-radius: 0 0 0.2em 0.2em;
          font-size: 1em;
        }
        bulb-on{
          opacity: 0;
          animation: glow 3s linear infinite;
        }
        @keyframes glow {
          0%{
            opacity: 1;
          }
          5%{
            opacity: 1;
          }
          70%{
            opacity: 1;
          }
          74%{
            opacity: 0;
          }
          80%{
            opacity: 1;
          }
          84%{
            opacity: 0;
          }
          90%{
            opacity: 1;
          }
          100%{
            opacity: 1;
          }
        }
       
       .css {
          font-weight: 600;
          line-height: 1;
          padding: 1em;
          width: 20px;
          height: 20px;
          border: 1px solid red;
          border-radius: 50%;
          color: red;
          background-color: black;
          cursor: pointer;
       }
       
       .css {
          box-shadow: 0 0 30vw 40vw rgba(241,244,0,0);
          animation: animate 3s infinite;
          
       }
       
       @keyframes animate {
          45%, 55% {background-color:0 0 0 3px rgba(241,244,0,1); 
                    box-shadow: 0 0 0 3px rgba(241,244,0,1); }
       }
       .move {
         position:relative;
         animation-duration: 1s;
         animation-name: mymove;
        animation-fill-mode: forwards;
      }
       @keyframes mymove {
        from {top: 50px;}

        to {top: -30px;}
      }
     
      .light {
        position: absolute;
        
        top: calc(50% - 50px);
        left: calc(50% - 50px);
        width: 100px;
        height: 50px;
        border-radius: 50%;
        background-color: white;
        box-shadow:
          0 0 60px 30px white,
          0 0 30px 10px white,
          0 0 200px 200px white;
          animation-duration: 1s;
         animation-name: light;
        animation-fill-mode: forwards;
      }
      @keyframes light {
        from {
          opacity: 0;
        }

        to {opacity: 1;}
      }
        `
      }</style>
    </>
  )
}
